if("${CMAKE_BUILD_TYPE}" STREQUAL "Sanitize")
    set(SANITIZER "address" CACHE STRING "Sanitizer type [address memory thread leak]")

    # Set flags for compile
    set(CMAKE_CXX_FLAGS_SANITIZE
            "-g -O0 -fsanitize=${SANITIZER}"
            CACHE STRING "Flags used by the C++ compiler during sanitizer builds."
            FORCE )
    set(CMAKE_C_FLAGS_SANITIZE
            "-g -O0 -fsanitize=${SANITIZER}"
            CACHE STRING "Flags used by the C compiler during sanitizer builds."
            FORCE )
    set(CMAKE_EXE_LINKER_FLAGS_SANITIZE
            ""
            CACHE STRING "Flags used for linking binaries during sanitizer builds."
            FORCE )
    set(CMAKE_SHARED_LINKER_FLAGS_SANITIZE
            ""
            CACHE STRING "Flags used by the shared libraries linker during sanitizer builds."
            FORCE )
    mark_as_advanced(
            CMAKE_CXX_FLAGS_SANITIZE
            CMAKE_C_FLAGS_SANITIZE
            CMAKE_EXE_LINKER_FLAGS_SANITIZE
            CMAKE_SHARED_LINKER_FLAGS_SANITIZE)

    if(NOT "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        message(FATAL_ERROR "Compiler is not clang! Aborting...")
    endif()

    add_custom_target(sanitize unitTests COMMENT "Run sanitization with ${SANITIZER}" VERBATIM)
endif()
