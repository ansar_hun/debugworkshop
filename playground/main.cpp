#include <iostream>
#include <string>
#include <set>
#include <vector>

struct MovieInfo
{
	enum Type {
			HU_2D = 1,
			HU_3D,
			EN_2D,
			EN_3D,
	};

	std::string name;
	Type type;
};

std::ostream &operator << (std::ostream &out, const MovieInfo::Type &a) {
	switch (a) {
		case MovieInfo::Type::HU_2D:
			out << "HU_2D";
			break;
		case MovieInfo::Type::HU_3D:
			out << "HU_3D";
			break;
		case MovieInfo::Type::EN_2D:
			out << "EN_2D";
			break;
		case MovieInfo::Type::EN_3D:
			out << "EN_3D";
			break;
	}

	return out;
}

class Ticketing
{
	public:
		Ticketing(const std::vector<MovieInfo *> &availableMovies);

		void printMoviesByType(MovieInfo::Type type);
		int getPrice(const std::string &name);

	protected:
		MovieInfo *getMovieInfo(const std::string &name);
		int priceForType(MovieInfo::Type type);

	protected:
		std::vector<MovieInfo *> availableMovies;
};

Ticketing::Ticketing(const std::vector<MovieInfo *> &availableMovies)
	: availableMovies(availableMovies)
{}

void Ticketing::printMoviesByType(MovieInfo::Type type)
{
	for (auto item : availableMovies) {
		if (item->type = type) {
			std::cout << '\t' << item->name << " [" << item->type << "] " << std::endl;
		}
	}
}

int Ticketing::getPrice(const std::string &name)
{
	MovieInfo *info = getMovieInfo(name);
	return priceForType(info->type);
}

MovieInfo* Ticketing::getMovieInfo(const std::string &name)
{
	MovieInfo *info = nullptr;

	for (auto item : availableMovies) {
		if (item->name == name) {
			info = item;
		}
	}

	return info;
}

int Ticketing::priceForType(MovieInfo::Type type)
{
	switch (type) {
		case MovieInfo::Type::HU_2D:
			return 1000;
			break;
		case MovieInfo::Type::HU_3D:
			return 2000;
			break;
		case MovieInfo::Type::EN_2D:
			break;
		case MovieInfo::Type::EN_3D:
			return 1800;
			break;
	}
}

int main(int argc, char *argv[])
{
	std::cout << "Playground project, have fun! ;)" << std::endl;

	std::vector<MovieInfo*> availableMovies {
		new MovieInfo {"IT", MovieInfo::Type::EN_2D},
		new MovieInfo {"Kingsman", MovieInfo::Type::HU_2D},
		new MovieInfo {"Flatliners", MovieInfo::Type::HU_3D},
	};

	Ticketing ticketing(availableMovies);

	std::cout << "Available movies [HU]" << std::endl;
	ticketing.printMoviesByType(MovieInfo::Type::HU_2D);
	ticketing.printMoviesByType(MovieInfo::Type::HU_3D);

	std::cout << "Available movies [EN]" << std::endl;
	ticketing.printMoviesByType(MovieInfo::Type::EN_2D);
	ticketing.printMoviesByType(MovieInfo::Type::EN_3D);

	std::string movieName = argv[1];

	std::cout << "Price for movie " << ticketing.getPrice(movieName) << std::endl;

	return 0;
}
