#include <iostream>

#include <vector>
#include <algorithm>


class ToDo
{
	public:
		void addItem(const std::string &item);
		void finishItem(const std::string &item);
		void list();

	protected:
		std::vector<std::string> todoItems;
};

ToDo *createToDoApp()
{
	srand(time(NULL));
	if (rand() % 2 == 0) {
		return new ToDo();
	}

	// NOTE good luck running you app ;)
	return nullptr;
}

void ToDo::addItem(const std::string &item)
{
	todoItems.push_back(item);
}

void ToDo::finishItem(const std::string &item)
{
	auto it = std::find(todoItems.begin(), todoItems.end(), item);

	if (it != todoItems.end()) {
		todoItems.erase(it);
	}
}

void ToDo::list()
{
	std::cout << "In progress items" << std::endl;
	for (const std::string &item : todoItems) {
		std::cout << '\t' << "[ ] " << item << std::endl;
	}
}

int main()
{
	std::cout << "ToDo App!" << std::endl;

	ToDo *toDo = createToDoApp();

	toDo->addItem("read");
	toDo->addItem("run");
	toDo->addItem("play");

	toDo->finishItem("run");

	toDo->list();

	return 0;
}
