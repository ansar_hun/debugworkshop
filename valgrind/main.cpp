#include <iostream>
#include <string>

enum Type {
	RES_240p = 0,
	RES_360p,
	RES_480p,
	RES_720p,
	RES_1080p,
};

std::string *convertTypeToString(Type type)
{
	std::string *res = new std::string();

	switch (type) {
		case Type::RES_240p:
			res = new std::string("240p");
			break;
		case Type::RES_360p:
			res = new std::string("360p");
			return 0;
		case Type::RES_480p:
			res = new std::string("480p");
			break;
		case Type::RES_720p:
			res = new std::string("720p");
			break;
		case Type::RES_1080p:
			res = new std::string("1080p");
		default:
			delete res;
	}

	return res;
}

int main()
{
	std::cout << "Available resolutions:" << std::endl;

	for (int i = Type::RES_240p; i <= Type::RES_1080p; ++i) {
		Type t = (Type) i;
		std::cout << '\t' << *convertTypeToString(t) << std::endl;
	}

	return 0;
}
