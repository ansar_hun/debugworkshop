#include <iostream>
#include <string>
#include <list>

int main(int argc, char *argv[])
{
	std::cout << "Check Storage!" << std::endl;

	std::string item = argv[1];

	std::list<std::string> availableStorage {
		"cpu",
		"ram",
		"gpu",
	};

	bool found = false;
	for (const std::string &storageItem : availableStorage) {
		if (storageItem == item) {
			found = true;
		} else {
			found = false;
		}
	}

	std::cout << item << (found ? " is " : " isn't " ) << "available in storage!" << std::endl;

	return 0;
}
